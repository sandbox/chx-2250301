Let's suppose we want to create a caching layer on top of the entity_modified module.

This needs to be added to settings.php:

$databases['default']['default']['driver'] = 'mysql_caching';
$databases['default']['default']['cache'] = array(
  'entity_modified' => array(
    'bin' => 'cache',
    'columns' => array('entity_type', 'entity_id'),
  ),
  'queries' => array(
    'SELECT modified FROM {entity_modified} WHERE entity_type = :entity_type AND entity_id = :entity_id' => array(
      'table' => 'entity_modified',
      'placeholders' => array('entity_type' => ':entity_type', 'entity_id' => ':entity_id'),
      'fields' => array('modified'),
    ),
  ),
);

And because of https://github.com/drush-ops/drush/issues/609 a very crude drush_issue_609_quickfix.patch is provided.

Also, to demonstrate the 'filter' feature in queries, here's a more contrived example:

$databases['default']['default']['cache'] = array(
  'entity_modified' => array(
    'bin' => 'cache',
    'columns' => array('entity_id'),
  ),
  'queries' => array(
    'SELECT modified FROM {entity_modified} WHERE entity_type = :entity_type AND entity_id = :entity_id' => array(
      'table' => 'entity_modified',
      'placeholders' => array('entity_id' => ':entity_id'),
      'filter' => array('entity_type' => ':entity_type'),
      'fields' => array('modified'),
    ),
  ),
);

Now the cache keys will become entity_modified:$entity_id and entity_type will
be filtered after the query is run. This is useful if you want to delete the
entries belonging to a certain $entity_id regardless of $entity_type.

Note:

If you use another bin, like 'cache_database', ensure that it is cleared during cache clears by implementing hook_flush_caches().
