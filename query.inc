<?php

/**
 * @file
 * Query code for the caching Mysql driver.
 */

include_once DRUPAL_ROOT . '/includes/database/mysql/query.inc';

class InsertQuery_mysql_caching extends InsertQuery_mysql {

  /**
   * @var DatabaseConnection_mysql_caching
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $cache_map = $this->connection->getCacheMap($this->table);

    if ($cache_map && !$this->defaultFields && !$this->fromQuery
      && !array_diff($cache_map['columns'], $this->insertFields)) {
      $this->updateCache($cache_map);
    }

    return parent::execute();
  }

  protected function updateCache($cache_map) {
    // insertFields / insertValues are numerically indexed. Find the keys
    // among the fields and record their corresponding numeric index.
    $keys = array_flip($cache_map['columns']);
    $key_indexes = array();
    $fields = $this->insertFields;
    foreach ($this->insertFields as $index => $field) {
      if (isset($keys[$field])) {
        $key_indexes[$field] = $index;
      }
    }

    // If every key was found in the fields then we can cache.
    if (count($key_indexes) == count($keys)) {
      foreach ($this->insertValues as $values) {
        $cache_keys = $keys;

        // Copy every key value pair into cache_keys.
        foreach ($key_indexes as $field => $index) {
          $cache_keys[$field] = $values[$index];
        }

        $cid = $this->connection->getCacheId($cache_map, $cache_keys);
        cache_set($cid, array_combine($fields, $values), $cache_map['bin'], $cache_map['expire']);
      }
    }
  }

}

class UpdateQuery_mysql_caching extends UpdateQuery {
  /**
   * @var bool Determines if this query can selectively invalidate the cache.
   */
  protected $cacheable = TRUE;

  /**
   * @var DatabaseCondition_mysql_caching
   */
  protected $condition;

  /**
   * @var DatabaseConnection_mysql_caching
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct(DatabaseConnection $connection, $table, array $options = array()) {
    parent::__construct($connection, $table, $options);
    $this->condition = new DatabaseCondition_mysql_caching('AND');
  }

  /**
   * {@inheritdoc}
   */
  public function expression($field, $expression, array $arguments = NULL) {
    $this->cacheable = FALSE;
    return parent::expression($field, $expression, $arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    if ($cache_map = $this->connection->getCacheMap($this->table)) {
      $cache = $this->condition->getCache($this->connection, $cache_map, $this->cacheable);
      if ($this->cacheable) {
        foreach ($cache as $cache_row) {
          cache_set($cache_row->cid, $this->fields + $cache_row->data, $cache_map['bin'], $cache_map['expire']);
        }
      }
      // As we have no idea what this query does, wipe the whole table.
      else {
        $this->connection->clearCache($cache_map, array(), TRUE);
      }
    }
    return parent::execute();
  }

}

class DeleteQuery_mysql_caching extends DeleteQuery {
  /**
   * @var DatabaseCondition_mysql_caching
   */
  protected $condition;

  /**
   * @var DatabaseConnection_mysql_caching
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct(DatabaseConnection $connection, $table, array $options = array()) {
    parent::__construct($connection, $table, $options);
    $this->condition = new DatabaseCondition_mysql_caching('AND');
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    if ($cache_map = $this->connection->getCacheMap($this->table)) {
      $cacheable = TRUE;
      $cache = $this->condition->getCache($this->connection, $cache_map, $cacheable);

      if ($cacheable) {
        if ($cache) {
          $this->connection->clearCacheIds($cache_map, array_keys($cache));
        }
      }
      // As we have no idea what this query does, wipe the whole table.
      else {
        $this->connection->clearCache($cache_map, array(), TRUE);
      }
    }
    return parent::execute();
  }
}

class TruncateQuery_mysql_caching extends TruncateQuery_mysql {
  /**
   * @var DatabaseConnection_mysql_caching
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function execute() {
    if ($cache_map = $this->connection->getCacheMap($this->table)) {
      $this->connection->clearCache($cache_map, array(), TRUE);
    }
    return parent::execute();
  }
}


class MergeQuery_mysql_caching extends MergeQuery {
  /**
   * @var DatabaseCondition_mysql_caching
   */
  protected $condition;

  /**
   * @var DatabaseConnection_mysql_caching
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct(DatabaseConnection $connection, $table, array $options = array()) {
    parent::__construct($connection, $table, $options);
    $this->condition = new DatabaseCondition_mysql_caching('AND');
  }
}
