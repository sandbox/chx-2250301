<?php

/**
 * @file
 * Installation code for the MySQL and caching driver.
 */

/**
 * Specifies installation tasks for MySQL caching driver.
 */
class DatabaseTasks_mysql_caching extends DatabaseTasks {
  /**
   * The PDO driver name for MySQL and equivalent databases.
   *
   * @var string
   */
  protected $pdoDriver = 'mysql';

  /**
   * Returns a human-readable name string for MySQL and equivalent databases.
   */
  public function name() {
    return st('MySQL, MariaDB, or equivalent - with caching on top');
  }
}
