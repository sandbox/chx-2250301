<?php

/**
 * @file
 * Database connection code for caching MySQL database servers.
 */

include_once DRUPAL_ROOT . '/includes/database/mysql/database.inc';

interface MySQLCachingInterface {

  /**
   * Implements DatabaseConnection_mysql::query().
   */
  public function parentQuery($query, array $args = array(), $options = array());

  /**
   * Retrieves the cache map from settings.php for a given table.
   *
   * @param string $table
   *   The name of the table.
   *
   * @return bool|array
   *   The cache map if it exists for the table, FALSE otherwise. The map
   *   contains columns, expire (defaults to CACHE_PERMANENT),
   *   bin (defaults to cache_database) and keys (defaults to array($table)).
   */
  public function getCacheMap($table);

  public function getCacheId($cache_map, $cache_keys = array());

  /**
   * Transforms a cid array into the first argument of cache_get_multiple().
   *
   * @param $cache_map
   *   The map returned from getCacheMap().
   * @param $cid_array
   *   An array keyed by fields. One field can have an array as a value, the
   *   rest are strings.
   *
   * @return array
   *   The flattened and prefixed cartesian product of $cid. Example:
   * @code
   * array(
   *  'field1' => 'foo',
   *  'field2' => array('bar', 'baz'),
   * )
   * @encode
   *
   * then the result will be
   * @code
   * array('prefix:foo:bar', 'prefix:foo:baz')
   * @endcode
   */
  public function getMultipleCacheIds($cache_map, $cache_keys, $multiple_cid_key = '');

  public function clearCache($cache_map, $cache_keys = array(), $wildcard = FALSE);

  public function clearCacheIds($cache_map, $cache_ids);
}

class DatabaseConnection_mysql_caching extends DatabaseConnection_mysql implements MySQLCachingInterface {

  /**
   * {@inheritdoc}
   */
  public function driver() {
    return 'mysql_caching';
  }

  /**
   * {@inheritdoc}
   */
  public function query($query, array $args = array(), $options = array()) {
    // Check if we can potentially cache this query.
    $query_cache_map = $this->getQueryCacheMap($query);

    if ($query_cache_map) {
      $cache_map = $this->getCacheMap($query_cache_map['table']);

      // Store data for further use.
      $cache_map['query'] = $query_cache_map;

      $query = $this->cachedQuery($query, $args, $options, $cache_map);
    }
    return parent::query($query, $args, $options);
  }

  // MySQL Caching Interface
  //------------------------------------------------------------------------

  /**
   * {@inheritdoc}
   */
  public function parentQuery($query, array $args = array(), $options = array()) {
    return parent::query($query, $args, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMap($table) {
    $connection_options = $this->getConnectionOptions();
    $cache_map_all = $connection_options['cache'];
    return isset($cache_map_all[$table]) ? $cache_map_all[$table] + array(
      'expire' => CACHE_PERMANENT,
      'bin' => 'cache_database',
      'keys' => array(
        $table,
      ),
      'query' => array(),
    ) : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheId($cache_map, $cache_keys = array()) {
    $sorted_keys = array();

    if (empty($cache_keys)) {
      return implode(':', $cache_map['keys']);
    }

    // Ensure that sort oder for cache ID is always the same.
    foreach ($cache_map['columns'] as $key) {
      if (!isset($cache_keys[$key]) && function_exists('watchdog')) {
        watchdog(
          'mysql_caching',
          'Warning: Undefined cache key @key during call to getCacheId',
          array(
            '@key' => $key,
          ),
          WATCHDOG_WARNING
        );

        $sorted_keys[$key] = 'mysql_caching_undefined';
        continue;
      }
      $sorted_keys[$key] = $cache_keys[$key];
    }

    return implode(':', array_merge($cache_map['keys'], $sorted_keys));
  }

  /**
   * {@inheritdoc}
   */
  public function getMultipleCacheIds($cache_map, $cache_keys, $multiple_cid_key = NULL) {
    $cids = array();

    if ($multiple_cid_key) {
      $multiple_values = $cache_keys[$multiple_cid_key];
      unset($cache_keys[$multiple_cid_key]);

      // The multiple key is at the end.
      foreach ($multiple_values as $i => $multiple_value) {
        $multiple_cache_keys = array_merge($cache_keys, array(
          $multiple_cid_key => $multiple_value
        ));
        $cids[$i] = $this->getCacheId($cache_map, $multiple_cache_keys);
      }
    }
    else {
      $cids[] = $this->getCacheId($cache_map, $cache_keys);
    }
    return $cids;
  }

  public function clearCacheIds($cache_map, $cache_ids) {
    cache_clear_all($cache_ids, $cache_map['bin']);
  }

  /**
   * {@inheritdoc}
   */
  public function clearCache($cache_map, $cache_keys = array(), $wildcard = FALSE) {
    $cid = $this->getCacheId($cache_map, $cache_keys);
    if ($wildcard) {
      cache_clear_all($cid . ':', $cache_map['bin'], TRUE);
    }
    else {
      cache_clear_all($cid, $cache_map['bin']);
    }
  }

  protected function getCacheKeys($cache_map, $data) {
    $cache_keys = array();

    foreach ($cache_map['columns'] as $key) {
      $cache_keys[$key] = $data[$key];
    }
    return $cache_keys;
  }

  // MySQL Query Caching Interface
  //------------------------------------------------------------------------

  /**
   * Retrieves the query cache map from settings.php for a given query.
   *
   * @param string|object $query
   *   The query to check for.
   *
   * @return bool|array
   *   The cache map if it exists for the query, FALSE otherwise. The query map
   *   contains table, keys, and optionally fields and filter.
   */
  protected function getQueryCacheMap($query) {
    // Early return.
    if (!is_string($query)) {
      return FALSE;
    }

    $connection_options = $this->getConnectionOptions();
    $cache_map_all = $connection_options['cache'];

    if (isset($cache_map_all['queries'][$query])) {
      return $cache_map_all['queries'][$query] + array(
        'fields' => array(),
        'filter' => array(),
      );
    }
  }

  protected function getQueryCachePlaceholderKeys($cache_map, $args) {
    $cache_keys = array();

    foreach ($cache_map['query']['placeholders'] as $key => $placeholder) {
      // This is guaranteed to succeed as we know the query contains the placeholders.
      $cache_keys[$key] = $args[$placeholder];
    }
    return $cache_keys;
  }

  protected function getQueryCacheMultipleKey($cache_map, $args) {
    foreach ($cache_map['query']['placeholders'] as $key => $placeholder) {
      if (is_array($args[$placeholder])) {
        return $key;
      }
    }
  }

  protected function getQueryCacheResults(&$cids, $cache_map, $fields = array()) {
    // Retrieve data from cache.
    $cache = cache_get_multiple($cids, $cache_map['bin']);

    $result = array();

    foreach ($cache as $cache_row) {
      $result[] = $cache_row->data;
    }

    // Remove cache misses from result.
    $result = array_filter($result);

    // Optionally filter results.
    // This needs to be done here, so that results that don't matched are
    // retrieved from the database and then the cache is updated.
    if ($cache_map['query']['filter']) {
      foreach ($result as $k => $row) {
        foreach ($cache_map['query']['filter'] as $field => $placeholder) {
          if ($row[$field] != $args[$placeholder]) {
            unset($result[$k]);
            break;
          }
        }
      }
    }

    return $result;
  }

  protected function setQueryCacheResults($result, $cache_map, $cid_remaining) {
    $found = array();

    // Cache the fresh results.
    foreach ($result as $row) {
      $cache_keys = $this->getCacheKeys($cache_map, $row);
      $cid = $this->getCacheId($cache_map, $cache_keys);
      $found[$cid] = TRUE;
      cache_set($cid, $row, $cache_map['bin'], $cache_map['expire']);
    }

    // Cache that database did return no results for given cache IDs.
    foreach ($cid_remaining as $cid) {
      if (!isset($found[$cid])) {
        cache_set($cid, array(), $cache_map['bin'], $cache_map['expire']);
      }
    }
  }

  protected function filterQueryCacheResults($result, $cache_map, $args) {
    // Filter by the defined fields.
    $fields = array_flip($cache_map['query']['fields']);
    if (!empty($fields)) {
      foreach ($result as $key => $row) {
        // Filter result by fields.
        $result[$key] = array_intersect_key($row, $fields);
      }
    }

    return $result;
  }

  protected function cachedQuery($query, array $args, array $options, array $cache_map) {
    $cache_keys = $this->getQueryCachePlaceholderKeys($cache_map, $args);
    $multiple_cid_key = $this->getQueryCacheMultipleKey($cache_map, $args);
    $cid_map = $this->getMultipleCacheIds($cache_map, $cache_keys, $multiple_cid_key);

    $cids = array_values($cid_map);

    // Retrieve cached data.
    $result = $this->getQueryCacheResults($cids, $cache_map);

    // Calculate remaining cache ids - array_intersect() preserves keys.
    $cid_remaining = array_intersect($cid_map, $cids);

    // Retrieve missing data.
    if ($cid_remaining) {
      if ($multiple_cid_key) {
        // Remove already calculated arguments.
        $placeholder = $cache_map['query']['placeholders'][$multiple_cid_key];
        $args[$placeholder] = array_intersect_key($args[$placeholder], $cid_remaining);
      }

      $new_options = $options;
      $new_options['fetch'] = PDO::FETCH_ASSOC;

      // Horrible hack to get every field in the results.
      // @todo Only add the fields necessary to produce the cache key.
      $query = preg_replace('/^SELECT/', 'SELECT *, ', $query);
      $query_result = parent::query($query, $args, $new_options)->fetchAll();

      $this->setQueryCacheResults($query_result, $cache_map, $cid_remaining);
      $result = array_merge($result, $query_result);
    }
    
    $result = $this->filterQueryCacheResults($result, $cache_map, $args);

    // Return the results.
    return new DatabaseStatementPrefetch_mysql_caching($this, $query, array(
      'data' => $result,
      'column_names' => $cache_map['query']['fields'],
    ));
  }

}

class DatabaseStatementPrefetch_mysql_caching extends DatabaseStatementPrefetch implements Iterator, DatabaseStatementInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct($connection, $query, array $driver_options = array()) {
    $this->data = $driver_options['data'];
    $this->resultRowCount = count($this->data);

    if ($this->resultRowCount) {
      $this->columnNames = $driver_options['column_names'] ?: array_keys($this->data[0]);
    }
    else {
      $this->columnNames = array();
    }

    // The actual row count always matches the result row count in our case.
    $this->rowCount = $this->resultRowCount;

    // No need to pass driver options on to parent.
    $driver_options = array();

    parent::__construct($connection, $query, $driver_options);
  }

  /**
   * {@inheritdoc}
   */
  public function execute($args = array(), $options = array()) {
    // Code copied from DatabaseStatementPrefetch::execute();
    if (isset($options['fetch'])) {
      if (is_string($options['fetch'])) {
        // Default to an object. Note: db fields will be added to the object
        // before the constructor is run. If you need to assign fields after
        // the constructor is run, see http://drupal.org/node/315092.
        $this->setFetchMode(PDO::FETCH_CLASS, $options['fetch']);
      }
      else {
        $this->setFetchMode($options['fetch']);
      }
    }

    // Nothing to be done here; data was already initialized in constructor.

    // Initialize the first row in $this->currentRow.
    $this->next();

    return TRUE;
  }
}

class DatabaseCondition_mysql_caching extends DatabaseCondition {

  /**
   * @var bool Determines if the condition can be converted into cache ids.
   */
  protected $isCacheable = TRUE;

  /**
   * {@inheritdoc}
   */
  public function condition($field, $value = NULL, $operator = NULL) {
    // Only allow equivalence operations. No operator leads to equivalence
    // except for NULL values.
    $this->isCacheable = $this->isCacheable && ((!isset($operator) && isset($value)) || $operator == '=' || $operator == 'IN' || ($field instanceof DatabaseCondition_mysql_caching && $field->isCacheable()));
    return parent::condition($field, $value, $operator);
  }

  /**
   * {@inheritdoc}
   */
  public function exists(SelectQueryInterface $select) {
    $this->isCacheable = FALSE;
    return parent::exists($select);
  }

  /**
   * {@inheritdoc}
   */
  public function notExists(SelectQueryInterface $select) {
    $this->isCacheable = FALSE;
    return parent::notExists($select);
  }

  /**
   * {@inheritdoc}
   */
  public function where($snippet, $args = array()) {
    $this->isCacheable = FALSE;
    return parent::where($snippet, $args);
  }

  /**
   * Determines if the condition can be converted into cache ids.
   *
   * @return bool
   *   Returns TRUE if cacheable, FALSE otherwise.
   */
  public function isCacheable() {
    return $this->isCacheable;
  }

  /**
   * @param DatabaseConnection_mysql_caching $connection
   * @param array $cache_map
   * @param bool $isCacheable
   * @return array
   */
  public function getCache($connection, array $cache_map, &$isCacheable) {
    $cache = array();
    if (!$this->isCacheable || !$isCacheable) {
      $isCacheable = FALSE;
      return $cache;
    }

    $cache_keys = array_flip($cache_map['columns']);
    $matches = 0;
    $multiple_cid_key = '';

    $conditions = $this->conditions;
    unset($conditions['#conjunction']);

    // Special case for merge queries: The first argument is a condition itself.
    if (count($conditions) == 1 && $conditions[0]['field'] instanceof DatabaseCondition_mysql_caching) {
      return $conditions[0]['field']->getCache($connection, $cache_map, $isCacheable);
    }

    $values_matches = array();
    foreach ($conditions as $condition) {
      if (isset($cache_keys[$condition['field']])) {
        if (is_array($condition['value'])) {
          $cache_keys[$condition['field']] = array_values($condition['value']);
          $multiple_cid_key = $condition['field'];
        }
        else {
          $cache_keys[$condition['field']] = $condition['value'];
        }
        $matches++;
      }
      else {
        if ($condition['operator'] == '=') {
          $condition['value'] = array($condition['value']);
        }
        $values_matches[$condition['field']] = $condition['value'];
      }
    }
    if ($matches == count($cache_map['columns'])) {
      $cids = $connection->getMultipleCacheIds($cache_map, $cache_keys, $multiple_cid_key);
      $cache = cache_get_multiple($cids, $cache_map['bin']);
      if ($values_matches) {
        foreach ($cache as $cid => $cache_row) {
          foreach ($values_matches as $field => $value) {
            if (!in_array($cache_row->data[$field], $value)) {
              unset($cache[$cid]);
              break;
            }
          }
        }
      }
    }
    else {
      $isCacheable = FALSE;
    }
    return $cache;
  }
}
